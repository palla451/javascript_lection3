function aggiungiNominativo(nominativo)
{


    // creo un tag li vuoto
     var tagLI = $('<li>');

    // inserisco il nominativo dentro il tag <li>
    tagLI.html(nominativo.toUpperCase());


    tagLI.attr({
        'title': 'titolo del tag',
        'data-id':'3'
    });

    tagLI.on('click', function () {
       var invitatoCliccato = $(this).html();
       var messaggio = 'Stai cliccando su '.concat(invitatoCliccato);
       alert(messaggio);
    });

    // Aggiungo questo tag <li> dentro il tag <ul #riepilogo>
    $('#riepilogo').append(tagLI);
}